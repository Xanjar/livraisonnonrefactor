
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        System.out.println(5);
        Thread.sleep(1000);
        System.out.println(4);
        Thread.sleep(1000);
        System.out.println(3);
        Thread.sleep(1000);
        System.out.println(2);
        Thread.sleep(1000);
        System.out.println(1);
        Thread.sleep(1000);
        System.out.println("Début du programme");

        List<Commande> commandes = new LinkedList<Commande>();
        System.out.println("Preparation des commandes");
        Restaurant restaurant = new Restaurant(commandes);
        restaurant.genererCommande();
        System.out.println("Tri des commande par ordre de priorité");
        restaurant.quickSort(0,4999);
        System.out.println(commandes.get(0).getPriorite());
        System.out.println("Livraison des commandes");
        Livreur livreur = new Livreur(commandes);
        System.out.println("Argent récolté : "+livreur.livrer());

        System.out.println(5);
        Thread.sleep(1000);
        System.out.println(4);
        Thread.sleep(1000);
        System.out.println(3);
        Thread.sleep(1000);
        System.out.println(2);
        Thread.sleep(1000);
        System.out.println(1);
        Thread.sleep(1000);
        System.out.println("Fin du programme");
    }

}

import java.util.List;
import java.util.Random;
import java.util.TreeMap;

public class Restaurant {
    private List<Commande> commandes;

    public Restaurant(List<Commande> commandes) {
        this.commandes = commandes;
    }

    public void genererCommande(){
        Random rand = new Random();
        for (int i = 0; i < 5000; i++) {
            TreeMap<Article,Integer> map = new TreeMap<Article,Integer>();
            map.put(Article.BURGER,rand.nextInt(10 + 1));
            map.put(Article.FRITES,rand.nextInt(10 + 1));
            map.put(Article.PATES,rand.nextInt(10 + 1));
            map.put(Article.SUSHI,rand.nextInt(10 + 1));
            map.put(Article.PIZZA,rand.nextInt(10 + 1));
            map.put(Article.TACOS,rand.nextInt(10 + 1));
            map.put(Article.TIRAMISU,rand.nextInt(10 - 1));
            this.commandes.add(new Commande(map,rand.nextInt((10 - 1) + 1) + 1,rand.nextInt((50 - 10) + 1) + 10));
        }
    }

    public void bubbleSort(){

        for (int i=0; i< this.commandes.size()-1;++i){

            for(int j=0; j < this.commandes.size()-i-1; ++j){

                if(commandes.get(j+1).getPriorite()<commandes.get(j).getPriorite()){

                    Commande swap = commandes.get(j);
                    commandes.set(j,commandes.get(j+1));
                    commandes.set(j+1,swap);

                }
            }
        }
    }

    public void insertSort()
    {
        int taille = this.commandes.size();

        for (int i = 1; i < taille; i++)
        {
            Commande index = this.commandes.get(i);
            int j = i-1;

            while(j >= 0 && this.commandes.get(j).getPriorite() > index.getPriorite())
            {
                this.commandes.set(j+1,this.commandes.get(j));
                j--;
            }
            this.commandes.set(j+1,index);
            System.out.println(i);

        }
    }

    public void quickSort(int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(begin, end);

            quickSort(begin, partitionIndex-1);
            quickSort(partitionIndex+1, end);
        }
    }
    private int partition(int begin, int end) {
        Commande pivot = this.commandes.get(end);
        int i = (begin-1);

        for (int j = begin; j < end; j++) {
            if (this.commandes.get(j).getPriorite() <= pivot.getPriorite()) {
                i++;

                Commande swapTemp = this.commandes.get(i);
                this.commandes.set(i,this.commandes.get(j));
                this.commandes.set(j,swapTemp);
            }
        }
        Commande swapTemp = this.commandes.get(i+1);
        this.commandes.set(i+1,this.commandes.get(end));
        this.commandes.set(end,swapTemp);

        return i+1;
    }

}

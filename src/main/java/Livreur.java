import java.util.List;

public class Livreur {
    private List<Commande> commandes;

    public Livreur(List<Commande> commandes) {
        this.commandes = commandes;
    }

    public int livrer(){
        int prix = 0;
        for (Commande c: this.commandes) {
            prix += c.getPrix();
        }
        return prix;
    }
}
